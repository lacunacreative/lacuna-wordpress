<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php bloginfo('template_directory');?>/favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <?php wp_head();?>
  </head>

  <body <?php body_class(); ?>>

<!-- Primary Naviation -->
  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="row">
                <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="<?php bloginfo( 'url' ); ?>">Bootstrap</a>
                </div>

                <div class="navbar-collapse collapse">
                
                  <?php 
                    $args = array(
                      'theme_location'        => 'main-menu',
                      'menu_class'  => 'nav navbar-nav',
                      'container'   => 'false'
                    );
                    wp_nav_menu( $args );
                  ?>
        
                </div><!--/.navbar-collapse -->
        </div>

      </div>
  </div>
  <!-- End of Navigation -->
